package com.videri.vchronyccmdsample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button chronycTracking = (Button)findViewById(R.id.send_chronyc_tracking_cmd_button);
        chronycTracking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.v(TAG, "chronycTracking button click. " );
                Log.v(TAG,  ShellManager.getInstance().runCommand("chronyc tracking").getResultString());

            }
        });

        Button chronycSources = (Button)findViewById(R.id.send_chronyc_sources_cmd_button);
        chronycSources.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.v(TAG, "chronycSources button click. " );
                Log.v(TAG,  ShellManager.getInstance().runCommand("chronyc sources").getResultString());

            }
        });

        Button chronycSourcestats = (Button)findViewById(R.id.send_chronyc_sourcestats_cmd_button);
        chronycSourcestats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.v(TAG, "chronycSourcestats button click. " );
                String command = "chronyc sourcestats";
                Log.v(TAG,  ShellManager.getInstance().runCommand(command).getResultString());

            }
        });
    }
}
