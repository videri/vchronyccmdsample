package com.videri.vchronyccmdsample;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Created by yiminglin on 9/20/16.
 */
public class ShellManager {
    private static ShellManager INSTANCE = new ShellManager();

    public static ShellManager getInstance() {
        return INSTANCE;
    }

    public Response runCommand(String command) {
        Process process = null;
        String result = null;
        int exitCode = -1;
        try {
            String[] cmd = {"sh", "-c", command};
            ProcessBuilder builder = new ProcessBuilder(cmd);
            builder.redirectErrorStream(true);
            process = builder.start();
            BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
            // read the output from the command
            StringBuilder sb = new StringBuilder();
            String s;
            while ((s = stdInput.readLine()) != null) {
                sb.append(s).append("\n");
            }
            result = sb.toString();
            exitCode = process.waitFor();
        } catch (Exception e) {
            if(result == null || result.isEmpty()) {
                result = e.getClass().getSimpleName() + " thrown\n" + e.getMessage();
            }
        }
        if(process != null) {
            process.destroy();
        }
        return new Response(result, exitCode);
    }

    public class Response {
        private final String resultStr;
        private final int exitCode;
        public Response(String resultStr, int exitCode) {
            this.resultStr = resultStr;
            this.exitCode = exitCode;
        }

        public String getResultString() {
            return this.resultStr;
        }

        public int getExitCode() {
            return this.exitCode;
        }
    }
}
