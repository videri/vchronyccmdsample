package com.videri.vchronyccmdsample;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by yiminglin on 9/20/16.
 */
public class NtpServerStat {
    //https://docs.fedoraproject.org/en-US/Fedora/18/html/System_Administrators_Guide/sect-Checking_if_chrony_is_synchronized.html
    //https://github.com/mlichvar/chrony/blob/1834ee05e5b808855f1802f88a5bdb1a634edfb1/client.c
    //Name/IP Address            NP  NR  Span  Frequency  Freq Skew  Offset  Std Dev
    //time.nrc.ca                35  23  586m     -0.012      0.082   -146us  1422us
    private final String name;
    private final int np;
    private final int nr;
    private final float spannedMinutes;
    //PPM
    private final float frequencyPpm;
    private final float skewPpm;
    private final float offsetMs;
    private final float stdDevMs;

    protected NtpServerStat(String[] stats) {
        name = stats[0];
        np = Integer.parseInt(stats[1]);
        nr = Integer.parseInt(stats[2]);
        spannedMinutes = convertToSeconds(stats[3]) / 60f;
        frequencyPpm = Float.parseFloat(stats[4]);
        skewPpm = Float.parseFloat(stats[5]);
        offsetMs = convertToSeconds(stats[6]) * 1000;
        stdDevMs = convertToSeconds(stats[7]) * 1000;
    }

    @Override
    public String toString() {
        return String.format(Locale.US, "Name: %s NP: %d NR: %d Spanned Minutes: %f Frequency PPM: %f Skew PPM: %f Offset MS: %f Std Deviation MS: %f", name, np, nr, spannedMinutes, frequencyPpm, skewPpm, offsetMs, stdDevMs);
    }

    public JSONObject toJson() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("name", name);
        jsonObject.put("np", np);
        jsonObject.put("nr", nr);
        jsonObject.put("spanned_minutes", spannedMinutes);
        jsonObject.put("freq_ppm", frequencyPpm);
        jsonObject.put("skew_ppm", skewPpm);
        jsonObject.put("offset_ms", offsetMs);
        jsonObject.put("std_dev_ms", stdDevMs);
        return jsonObject;
    }


    private static float convertToSeconds(String time) {
        String[] timeInfo = time.split("(?<=\\d)(?=\\D)");
        float timeInSeconds = Float.parseFloat(timeInfo[0]);
        if (timeInfo.length < 2) {
            return timeInSeconds;
        }
        float factor = 1;
        switch (timeInfo[1]) {
            case "ns":
                factor /= 1000000000;
                break;
            case "us":
                factor /= 1000000;
                break;
            case "ms":
                factor /= 1000;
                break;
            case "s":
                factor = 1;
                break;
            case "m":
                factor = 60;
                break;
            case "h":
                factor = 60 * 60;
                break;
            case "d":
                factor = 60 * 60 * 24;
                break;
            case "y":
                factor = 60 * 60 * 24 * 365;
                break;
        }
        return timeInSeconds * factor;
    }

    private static List<NtpServerStat> parseChronyStats(String message) {
        ArrayList<NtpServerStat> serverStats = new ArrayList<>();
        String[] serverInfo = message.split("\\n+");
        for (String ntpServer : serverInfo) {
            String[] metrics = ntpServer.split("\\s+");
            NtpServerStat stat = new NtpServerStat(metrics);
            serverStats.add(stat);
        }
        return serverStats;
    }

    public static List<NtpServerStat> getChronySourceStats() {
        try {
            ShellManager.Response shellResponse = ShellManager.getInstance().runCommand("chronyc sourcestats|busybox tail -n +4");
            if (shellResponse.getExitCode() == 0) {
                return parseChronyStats(shellResponse.getResultString());
            }
        } catch (Exception e) {
            Log.v("NtpServerStat","Exception trying to get chrony stats!", e);
        }
        return null;
    }
}
